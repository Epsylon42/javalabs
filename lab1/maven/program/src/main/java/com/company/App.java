package com.company.program;

import com.company.circle.Circle;
import org.javatuples.Pair;

public class App {
    public static void main( String[] args ) {
        Circle circle = new Circle(new Pair(0.0f, 0.0f), 5.0f);
        System.out.println(circle.area());
    }
}
