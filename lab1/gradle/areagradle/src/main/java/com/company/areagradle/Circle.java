package com.company.areagradle;

import org.javatuples.Pair;

public class Circle {
    Pair<Float, Float> center;
    float radius;

    public Circle(Pair<Float, Float> center, float radius) {
        this.center = center;
        this.radius = radius;
    }

    public float area() {
        return 3.14f * radius * radius;
    }
}
