import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.pavel.lab2.DAO;
import com.pavel.lab2.DAOImpl;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DAOTest {
    Connection conn;
    DAO dao;

    @BeforeAll
    void connect() throws SQLException {
        System.out.println(System.getProperty("user.dir"));

        conn = DriverManager
                .getConnection("jdbc:sqlite:src/test/resources/test.db");

        dao = new DAO(conn);
    }

    @Test
    void getOneUser() throws SQLException {
        var user = dao.getUser(0);
        assertEquals((Integer)0, user.id);
        assertEquals("user", user.username);
        assertNull(user.email);
    }

    @Test
    void getUserList() throws SQLException {
        var users = dao.getUserList();
        assertEquals(3, users.size());

        var usr = users
                .stream()
                .filter(user -> user.id == 2)
                .findFirst()
                .get();

        assertEquals("user2", usr.username);
        assertEquals("email@gmail.com", usr.email);
    }

    @Test
    void getAudio() throws SQLException {
        var audio = dao.getAudio(1);
        assertEquals((Integer)44100, audio.sampleRate);

        assertEquals(1, dao.getAudioList().size());
    }

    @Test
    void getVideo() throws SQLException {
        var video = dao.getVideo(2);
        assertEquals(video.thumbnailUrl, "https://abc/thumb.png");

        assertEquals(2, dao.getVideoList().size());
    }

    @Test
    void getNonexistent() throws SQLException {
        assertThrows(RuntimeException.class, () -> dao.getUser(5));
        assertThrows(RuntimeException.class, () -> dao.getVideo(1));
        assertThrows(RuntimeException.class, () -> dao.getAudio(2));
    }

    @Test
    void invalidModel() throws SQLException {
        assertThrows(SQLException.class, () -> new DAOImpl(conn).getObjectList(NoAnnotation.class));
        assertThrows(RuntimeException.class, () -> new DAOImpl(conn).getObject(NoPrimaryKey.class, 0));
    }
}
