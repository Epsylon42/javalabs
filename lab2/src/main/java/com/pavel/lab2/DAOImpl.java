package com.pavel.lab2;

import javafx.scene.control.Tab;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class DAOImpl {
    Connection conn;

    public DAOImpl(Connection connection) {
        this.conn = connection;
    }

    public <T> T getObject(Class<T> model, int id) throws SQLException {
        var desc = new TableDesc(model);
        String sql = "SELECT "
                + desc.getSelectList().collect(Collectors.joining(", "))
                + " FROM "
                + desc.getTable()
                + " WHERE "
                + desc.getObjectPrimary() + " = ?;";

        var stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);

        var res = stmt.executeQuery();
        if (!res.next()) {
            throw new RuntimeException(model.getName() + " with id " + id + " not found");
        }

        return constructObject(model, desc, res);
    }

    public <T> List<T> getObjectList(Class<T> model) throws SQLException {
        var desc = new TableDesc(model);
        String sql = "SELECT "
                + desc.getSelectList().collect(Collectors.joining(", "))
                + " FROM "
                + desc.getTable() + ";";

        var stmt = conn.prepareStatement(sql);
        var res = stmt.executeQuery();

        var list = new ArrayList<T>();
        while (res.next()) {
            list.add(constructObject(model, desc, res));
        }

        return list;
    }

    private <T> T constructObject(Class<T> model, TableDesc desc, ResultSet row) throws SQLException {
        T obj;
        try {
            obj = model.getConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Could not create an instance of " + model.getName());
        }

        try {
            var fields = desc.getFields().collect(Collectors.toList());
            for (var fieldDesc : fields) {
                var field = model.getField(fieldDesc.nameInClass);
                if (field.getType() == Integer.class) {
                    field.set(obj, row.getInt(fieldDesc.nameInClass));
                } else if (field.getType() == String.class) {
                    field.set(obj, row.getString(fieldDesc.nameInClass));
                } else {
                    throw new RuntimeException("Fields of type " + field.getType().getName() + " are not supported");
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Could not set " + model.getName() + " field");
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Unreachable");
        }

        return obj;
    }
}

class FieldDesc {
    public String nameInClass;
    public String nameInTable;
}

class TableDesc {
    public Optional<TableDesc> parent = Optional.empty();

    public String tableName;
    public List<String> fields = new ArrayList<>();
    public Optional<String> primary = Optional.empty();

    public TableDesc(Class model) {
        var modelAnnotation = (Model)model.getDeclaredAnnotation(Model.class);
        if (modelAnnotation != null) {
            tableName = modelAnnotation.table();
        } else {
            tableName = model.getName();
        }

        for (var field : model.getDeclaredFields()) {
            fields.add(field.getName());
            var primaryAnnotation = (PrimaryKey)field.getDeclaredAnnotation(PrimaryKey.class);
            if (primaryAnnotation != null) {
                primary = Optional.of(field.getName());
            }
        }

        var sc = model.getSuperclass();
        if (sc != Object.class) {
            parent = Optional.of(new TableDesc(sc));
        } else {
            parent = Optional.empty();
        }
    }

    public String getTablePrimary() {
        return tableName + "." + primary.orElse("__com_pavel_lab__id");
    }

    public String getObjectPrimary() {
        return primary
                .map(p -> tableName + "." + p)
                .orElseGet(() -> parent
                        .orElseThrow(() -> new RuntimeException("No primary key in object hierarchy"))
                        .getObjectPrimary());
    }

    public Stream<FieldDesc> getFields() {
        var fieldsStream = fields
                .stream()
                .map(field -> {
                    var desc = new FieldDesc();
                    desc.nameInClass = field;
                    desc.nameInTable = tableName + "." + field;
                    return desc;
                });

        return parent
                .map(TableDesc::getFields)
                .map(parentStream -> Stream.concat(parentStream, fieldsStream))
                .orElse(fieldsStream);
    }

    public Stream<String> getSelectList() {
        return getFields().map(desc -> desc.nameInTable);
    }

    public String getTable() {
        return parent
                .map(TableDesc::getTable)
                .map(parentTable ->
                        "(" +
                                parentTable +
                                " INNER JOIN " +
                                tableName +
                                " ON " +
                                getTablePrimary() + " = " + parent.get().getTablePrimary() +
                                ")"
                )
                .orElse(tableName);
    }
}
