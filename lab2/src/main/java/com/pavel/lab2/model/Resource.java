package com.pavel.lab2.model;

import com.pavel.lab2.Model;
import com.pavel.lab2.PrimaryKey;

@Model(table = "resource")
public class Resource {
    @PrimaryKey
    public Integer id;
    public Integer owner;
    public String url;
    public String name;
}
