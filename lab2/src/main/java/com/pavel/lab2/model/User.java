package com.pavel.lab2.model;

import com.pavel.lab2.*;

@Model(table = "users")
public class User {
    @PrimaryKey
    public Integer id;
    public String username;
    public String email;
}
