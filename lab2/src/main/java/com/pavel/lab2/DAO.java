package com.pavel.lab2;

import com.pavel.lab2.model.Audio;
import com.pavel.lab2.model.User;
import com.pavel.lab2.model.Video;

import java.sql.SQLException;
import java.util.List;
import java.sql.Connection;

public class DAO {
    DAOImpl impl;

    public DAO(Connection connection) {
        impl = new DAOImpl(connection);
    }

    public User getUser(int id) throws SQLException {
        return impl.getObject(User.class, id);
    }

    public List<User> getUserList() throws SQLException {
        return impl.getObjectList(User.class);
    }

    public Video getVideo(int id) throws SQLException {
        return impl.getObject(Video.class, id);
    }

    public List<Video> getVideoList() throws SQLException {
        return impl.getObjectList(Video.class);
    }

    public Audio getAudio(int id) throws SQLException {
        return impl.getObject(Audio.class, id);
    }

    public List<Audio> getAudioList() throws SQLException {
        return impl.getObjectList(Audio.class);
    }
}
