import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import com.pavel.lab3.*;
import static com.pavel.lab3.FuncSetUtils.*;


public class FuncSetTest {
    @Test
    void testEmpty() {
        assertTrue(forall(empty(), x -> false));
    }

    @Test
    void testSingleton() {
        assertTrue(forall(singleton(10), x -> x.equals(10)));
    }

    @Test
    void testUnion() {
        var set = union(singleton(10), singleton(15));
        assertTrue(set.contains(10));
        assertTrue(set.contains(15));
        assertFalse(set.contains(0));
    }

    @Test
    void testIntersection() {
        PureFuncSet<Integer> set1 = x -> 0 <= x && x <= 2;
        var set = intersect(set1, singleton(1));
        assertTrue(forall(set, x -> x.equals(1)));
    }

    @Test
    void testDiff() {
        PureFuncSet<Integer> set1 = x -> 0 <= x && x <= 3;
        PureFuncSet<Integer> set2 = x -> 2 <= x && x <= 10;
        assertTrue(forall(diff(set1, set2), x -> x < 2));
    }

    @Test
    void testFilter() {
        PureFuncSet<Integer> set = x -> 0 <= x && x <= 100;
        set = filter(set, x -> x % 2 == 0);
        for (int i = 0; i <= 100; i += 2) {
            assertTrue(set.contains(i));
        }
        for (int i = 1; i < 100; i += 2) {
            assertFalse(set.contains(i));
        }
    }

    @Test
    void testForall() {
        PureFuncSet<Integer> set = x -> 0 <= x && x <= 100;
        assertTrue(forall(set, x -> 0 <= x && x <= 100));
    }

    @Test
    void testExists() {
        PureFuncSet<Integer> set1 = x -> 0 <= x && x <= 100;
        PureFuncSet<Integer> set = union(set1, singleton(-5));
        assertTrue(exists(set, x -> x.equals(-5)));
        assertTrue(exists(set, x -> x.equals(50)));
    }

    @Test
    void testMap() {
        PureFuncSet<Integer> set = x -> 0 <= x && x <= 100;
        PureFuncSet<Double> mapped = map(set, x -> (double)x + 0.5);
        assertTrue(mapped.contains(1.5));
        assertTrue(mapped.contains(100.5));
        assertFalse(mapped.contains(0.0));
    }
}
