package com.pavel.lab3;


public interface PureFuncSet<T> {
    boolean contains(T element);
}
