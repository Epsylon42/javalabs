package com.pavel.lab3;

import java.util.function.Function;
import java.util.function.Predicate;


public class FuncSetUtils {
    public static <T> PureFuncSet<T> empty() {
        return x -> false;
    }

    public static <T> PureFuncSet<T> singleton(T element) {
        return x -> x.equals(element);
    }

    public static <T> PureFuncSet<T> union(PureFuncSet<T> a, PureFuncSet<T> b) {
        return x -> a.contains(x) || b.contains(x);
    }

    public static <T> PureFuncSet<T> intersect(PureFuncSet<T> a, PureFuncSet<T> b) {
        return x -> a.contains(x) && b.contains(x);
    }

    public static <T> PureFuncSet<T> diff(PureFuncSet<T> a, PureFuncSet<T> b) {
        return x -> a.contains(x) && !b.contains(x);
    }

    public static <T> PureFuncSet<T> filter(PureFuncSet<T> a, Predicate<T> p) {
        return x -> a.contains(x) && p.test(x);
    }

    public static boolean forall(PureFuncSet<Integer> a, Predicate<Integer> p) {
        return forallHelper(-1000, a, p);
    }

    private static boolean forallHelper(int i, PureFuncSet<Integer> a, Predicate<Integer> p) {
        if (i > 1000) {
            return true;
        } else if (!a.contains(i) || p.test(i)) {
            return forallHelper(i + 1, a, p);
        } else {
            return false;
        }
    }

    public static boolean exists(PureFuncSet<Integer> a, Predicate<Integer> p) {
        return existsHelper(-1000, a, p);
    }

    private static boolean existsHelper(int i, PureFuncSet<Integer> a, Predicate<Integer> p) {
        if (i > 1000) {
            return false;
        } else if (!a.contains(i) || !p.test(i)) {
            return existsHelper(i + 1, a, p);
        } else {
            return true;
        }
    }

    public static <T> PureFuncSet<T> map(PureFuncSet<Integer> a, Function<Integer, T> f) {
        return x -> exists(a, y -> f.apply(y).equals(x));
    }
}
